# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

unless AdminUser.exists?
  AdminUser.create!(email: 'admin_user@test.com', password: '123123')
  AdminUser.create!(email: 'admin_user_1@test.com', password: '123123')
  AdminUser.create!(email: 'admin_user_2@test.com', password: '123123')
end

unless Product.exists?
  Product.create!(name: 'The Invisalign', unit_price: 10_000)
  Product.create!(name: 'iTero intraoral scanners', unit_price: 30_000)
end

unless User.exists?
  User.create!(first_name: 'Elizabeth', last_name: 'Brown', email: 'user1@test.com', phone: "0909000001")
  User.create!(first_name: 'John', last_name: 'Wayne', email: 'user2@test.com', phone: "0909000002")
  User.create!(first_name: 'Eric', last_name: 'Arthur', email: 'user3@test.com', phone: "0909000003")
  User.create!(first_name: 'Wade', last_name: 'Hunter', email: 'user4@test.com', phone: "0909000004")
  User.create!(first_name: 'Dan', last_name: 'Kingston', email: 'user5@test.com', phone: "0909000005")
  User.create!(first_name: 'Roberto', last_name: 'Sandy', email: 'user6@test.com', phone: "0909000006")
end

unless Order.exists?
  Order.create!(user_id: User.find_by(email: 'user1@test.com').id, status: 2, amount: 50_000)
  Order.create!(user_id: User.find_by(email: 'user1@test.com').id, amount: 10_000)
  Order.create!(user_id: User.find_by(email: 'user1@test.com').id, amount: 30_000)
  Order.create!(user_id: User.find_by(email: 'user2@test.com').id, status: 2, amount: 30_000)
  Order.create!(user_id: User.find_by(email: 'user2@test.com').id, amount: 10_000)
  Order.create!(user_id: User.find_by(email: 'user3@test.com').id, status: 3, amount: 10_000)
  Order.create!(user_id: User.find_by(email: 'user4@test.com').id, status: 1, amount: 30_000)
  Order.create!(user_id: User.find_by(email: 'user1@test.com').id, status: 1, amount: 10_000)
end

unless OrderDetail.exists?
  OrderDetail.create!(order_id: Order.first.id, product_id: Product.first.id, unit_price: Product.first.unit_price, quanlity: 2, product_raw_data: Product.first.to_json)
  OrderDetail.create!(order_id: Order.first.id, product_id: Product.second.id, unit_price: Product.second.unit_price, quanlity: 1, product_raw_data: Product.second.to_json)
  OrderDetail.create!(order_id: Order.second.id, product_id: Product.first.id, unit_price: Product.first.unit_price, quanlity: 1, product_raw_data: Product.first.to_json)
  OrderDetail.create!(order_id: Order.third.id, product_id: Product.second.id, unit_price: Product.second.unit_price, quanlity: 1, product_raw_data: Product.second.to_json)
  OrderDetail.create!(order_id: Order.first(4).last.id, product_id: Product.second.id, unit_price: Product.second.unit_price, quanlity: 1, product_raw_data: Product.second.to_json)
  OrderDetail.create!(order_id: Order.first(5).last.id, product_id: Product.first.id, unit_price: Product.first.unit_price, quanlity: 1, product_raw_data: Product.first.to_json)
  OrderDetail.create!(order_id: Order.last(3).first.id, product_id: Product.first.id, unit_price: Product.first.unit_price, quanlity: 1, product_raw_data: Product.first.to_json)
  OrderDetail.create!(order_id: Order.last(2).first.id, product_id: Product.second.id, unit_price: Product.second.unit_price, quanlity: 1, product_raw_data: Product.second.to_json)
  OrderDetail.create!(order_id: Order.last.id, product_id: Product.first.id, unit_price: Product.first.unit_price, quanlity: 1, product_raw_data: Product.first.to_json)
end
