class CreateOrderDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :order_details do |t|
      t.references :product
      t.references :order
      t.float :unit_price, null: false
      t.integer :quanlity, null: false
      t.text :product_raw_data

      t.timestamps
    end
  end
end
