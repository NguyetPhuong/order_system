class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.integer :status, default: 0
      t.references :user
      t.float :amount, null: false, default: 0

      t.timestamps

      t.index :status
    end
  end
end
