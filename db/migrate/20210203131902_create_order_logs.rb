class CreateOrderLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :order_logs do |t|
      t.references :admin_user, null: false, foreign_key: true
      t.references :order, null: false, foreign_key: true
      t.text :message_raw_data

      t.timestamps
    end
  end
end
