class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.integer :sex
      t.datetime :birthday

      t.timestamps

      t.index :email, unique: true
    end
  end
end
