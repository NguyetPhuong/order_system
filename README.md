# README
## Order System
---
### 1. About system
Built with:

* Ruby - 2.7.0
* Rails - 6.0.3
* Yarn
* Webpack
* jQuery
* Bootstrap
*
### 2. ERD
![N|Solid](https://user-images.githubusercontent.com/43635763/106903378-7e4b0600-672c-11eb-9f8b-9e0d090df32b.png)

### 3. Installation
```sh
$ cd order_system
$ bundle install
$ yarn add bootstrap@4.3.1 jquery popper.js
$ rails db:create && rails db:create && rails db:migrate && rails db:seed
$ rails s
```
