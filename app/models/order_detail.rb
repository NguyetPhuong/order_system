class OrderDetail < ApplicationRecord
  belongs_to :product_form_db, class_name: "Product", foreign_key: "product_id"

  def product
    @product ||= Product.new.tap do |product|
      if product_raw_data
        saved_hash = JSON.parse(product_raw_data)
        product.attributes = saved_hash
        product.id = saved_hash['id']
      end
      product.readonly!
    end
  end

  def product=(product)
    product_raw_data = product.to_json
  end

  def amount
    unit_price * quanlity
  end
end
