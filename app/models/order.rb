class Order < ApplicationRecord
  belongs_to :user

  has_many :order_details

  enum status: {created: 0, confirming: 1, sending: 2, done: 3, canceled: 4 }

  delegate :email, to: :user, prefix: true
end
