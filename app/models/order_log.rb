class OrderLog < ApplicationRecord
  belongs_to :admin_user
  belongs_to :order

  def messages
    JSON.parse(message_raw_data.presence || "{}")
  end

end
