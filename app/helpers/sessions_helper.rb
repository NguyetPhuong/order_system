module SessionsHelper
  def log_in admin_user
    session[:admin_user_id] = admin_user.id
  end

  def log_out
    session.delete :admin_user_id
  end

  def current_admin_user
    return nil if session[:admin_user_id].blank?
    @current_admin_user ||= AdminUser.find(session[:admin_user_id])
  end
end
