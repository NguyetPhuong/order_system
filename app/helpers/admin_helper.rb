module AdminHelper
  def generate_breadcrum(title = '', bcrum_detail = [])
    lst = []
    lst << "<h1 class='mt-4'>#{title.capitalize}</h1>" if title.present?
    if bcrum_detail.is_a?(Array) && bcrum_detail.present?
      lst << "<ol class='breadcrumb mb-4'>"
      bcrum_detail_size = bcrum_detail.size - 1
      bcrum_detail.each_with_index do |breadcrumb, idx|
        active = idx == bcrum_detail_size ? 'active' : ''
        lst << "<li class='breadcrumb-item #{active}'>#{breadcrumb.to_s.capitalize}</li>"
      end
      lst << "</ol>"
    end
    lst.join
  end
end
