class SessionsController < ApplicationController
  include SessionsHelper
  before_action :current_admin_user_present, only: %w[new create]


  def new; end

  def create
    user = AdminUser.find_by email: params[:session][:email].downcase
    if user && user.authenticate(params[:session][:password])
      flash[:success] = "Login success"
      log_in user
      redirect_back fallback_location: admin_root_path
    else
      flash[:danger] = "Invalid email/password combination"
      render :new
    end
  end

  def destroy
    log_out
    flash[:success] = "You are logged out"
    redirect_to login_path
  end

  private

  def current_admin_user_present
    return redirect_to admin_root_path if current_admin_user.present?
  end
end
