class Admin::OrdersController < Admin::BaseController
  before_action :set_order, only: %i[ show edit update destroy ]
  before_action :set_breadcrum

  # GET /orders
  def index
    @orders = SearchServices::Order.call(search_params)
  end

  # GET /orders/1
  def show
    @bcrum_detail = ["Order", "detail", @order.id]
  end

  # GET /orders/1/edit
  def edit
    @bcrum_detail = ["Order", "edit", @order.id]
  end

  # PATCH/PUT /orders/1
  def update
    o_status = @order.status
    if @order.update(order_params)
      LogServices::OrderStatus.call(current_admin_user, order: @order, o_status: o_status)
      OrdersMailer.change_status(@order).deliver_now!
      redirect_to admin_order_path(@order), notice: "Order was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private
    def set_order
      @order = Order.find(params[:id]).decorate
    end

    def order_params
      params.require(:order).permit(:status)
    end

    def search_params
      s_params = params[:search]
      s_params = s_params.present? ? s_params.to_unsafe_h : {}
      s_params.merge!(page: params['page']) if params['page'].present?
      s_params
    end

    def set_breadcrum
      @bcrum_title = "Order"
      @bcrum_detail = ["Order"]
    end
end
