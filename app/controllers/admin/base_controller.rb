class Admin::BaseController < ActionController::Base
  include SessionsHelper
  layout 'admin'
  protect_from_forgery with: :exception
  before_action :logged_in?

  private

  def logged_in?
    return redirect_to login_path unless current_admin_user
  end
end
