# frozen_string_literal: true

module LogServices
  module Bases
    class Base
      def initialize(admin_user, **options)
        @admin_user = admin_user
        @options = options || {}
      end

      def update
        raise
      end

      class << self
        def call(admin_user, **options)
          log_service = new(admin_user, options || {})
          log_service.update
        end
      end
    end
  end
end
