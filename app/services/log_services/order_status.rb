# frozen_string_literal: true

module LogServices
  class OrderStatus < Bases::Base
    def update
      message_raw_data = order_log.message_raw_data
      message_raw_data ||= "{}"
      message_raw_data = JSON.parse(message_raw_data)
      message_raw_data[Time.current] = "AdminUser #{@admin_user.email} change status from #{o_status} to #{order.status}"
      order_log.message_raw_data = message_raw_data.to_json
      order_log.save
    end

    def order_log
      @order_log ||= OrderLog.find_or_initialize_by(admin_user: @admin_user, order: order)
    end

    private

    def order
      @order = @options.fetch(:order, nil)
    end

    def o_status
      @o_status = @options.fetch(:o_status, nil)
    end

    class << self
      def call(admin_user, **options)
        log_service = new(admin_user, options)
        log_service.update
      end
    end
  end
end
