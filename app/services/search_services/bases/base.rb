# frozen_string_literal: true

module SearchServices
  module Bases
    class Base
      attr_accessor :page

      def initialize(s_params)
        @s_params = s_params || {}
        @page = @s_params[:page] || 1
      end

      def result
        return @result if defined?(@result)
        @result = search
        @result = @result.includes(includes.presence)
        @result
      end

      def paginate
        result.page(self.page).per(50).decorate
      end

      private

      def search
        raise
      end

      def includes; end

      class << self
        def call(s_params = {})
          search_service = new(s_params)
          search_service.paginate
        end
      end
    end
  end
end
