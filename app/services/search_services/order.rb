# frozen_string_literal: true

module SearchServices
  class Order < Bases::Base
    def search
      order = ::Order.all
      order = order.where(status: @s_params[:status]) if @s_params[:status].present?
      order = order.joins(:user).where("email like ?", "%#{@s_params[:email]}%") if @s_params[:email].present?
      order
    end

    def includes
      :user
    end
  end
end
