class OrdersMailer < ApplicationMailer
  def change_status(order)
    @order = order

    mail(to: @order.user_email, subject: "Your order's status has been change!")
  end
end
