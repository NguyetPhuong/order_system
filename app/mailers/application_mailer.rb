class ApplicationMailer < ActionMailer::Base
  default from: 'no_reply@order_system.com'
  layout 'mailer'
end
